import os
import torch
import cv2
import pandas
import mysql.connector
from datetime import datetime
import multiprocessing as mp
import paho.mqtt.client as mqtt


#Esta función se encarga de insertar todas las fotos 
#   contenidas en FOTOS_TFG en la DDBB
def insert_into_database(input_folder, input_queue):
    while True:

        #Realizamos la conexión con la DDBB
        db = mysql.connector.connect(
            #LOS SIGUIENTES PARAMETROS DEBERÁN DE MODIFICARSE SEGÚN EL USUARIO
            
            host="HOST DDBB",
            user="USUARIO DDBB",
            password="CONTRASEÑA DDDB",
            database="NOMBRE DDBB"
        )
        cursor = db.cursor()

        # Obtenemos la lista de fotos en la carpeta, 
        #   usamos "os" para convertir una ruta en otra 
        #       que sea legible en Windows
        photo_list = os.listdir(input_folder)

        #Recorremos las fotos de la carpeta
        for photo in photo_list:
            #Usamos "join" para juntar una ruta y una foto 
            #   en este caso para convertirlas en una ruta legible en Windows
            image_path = os.path.join(input_folder, photo)

            # Verificar si la foto ya existe en la base de datos
            cursor.execute("SELECT * FROM fotos WHERE ruta = %s", (image_path,))
            result = cursor.fetchone()

            # Si la foto no existe en la base de datos, insertarla
            if not result:
                date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                query = "INSERT INTO fotos (ruta, fecha) VALUES (%s, %s)"
                cursor.execute(query, (image_path, date))
                db.commit()
                print("Foto insertada en la tabla 'fotos':", image_path)

                #Insertamos la foto en la cola
                input_queue.put(photo)

        # Cerrar los cursores y la conexión a la base de datos
        cursor.close()
        db.close()

#Esta función es la encargada de procesar las imágenes insertadas en la base de datos,
#   para pasar una inteligencia artificial que detecta armas e inserta las fotos en DDBB
#       avisando por MQTT a la placa de una detección
def detect_objects(input_queue, output_folder):

    # Código de detección de objetos utilizando YOLOv5
    model = torch.hub.load('ultralytics/yolov5', 'custom', path='(RUTA RAIZ DE LA CARPETA)/sistema-de-proteccion-para-las-fuerzas-del-orden/IA/armas.pt') #CAMBIAR SEGÚN USUARIO
    processed_images = []  # Lista para almacenar los nombres de las imágenes procesadas
    while True:
        db = mysql.connector.connect(
            #LOS SIGUIENTES PARAMETROS DEBERÁN DE MODIFICARSE SEGÚN EL USUARIO
            host="HOST DDBB",
            user="USUARIO DDBB",
            password="CONTRASEÑA DDDB",
            database="tfg_database"
        )

        #Obtenemos la foto de la cola enviada por la función anterior
        image_file = input_queue.get() 
        print("La foto obtenida de la cola es:",image_file)

        if image_file is None:
            break
        
        #Conexiones MQTT
        broker = "IP O DIRECCION MQTT"  # Dirección IP o nombre de host del broker MQTT, CAMBIAR SEGÚN USUARIO
        port = 1883  # Puerto del broker MQTT
        topic = "TEMA MQTT"  # Topic al que se enviará el mensaje, CAMBIAR SEGÚN USUARIO
        client = mqtt.Client() # Crear instancia del cliente MQTT

        #Definimos las carpetas de entrada y salida de imágenes
        input_folder = '(RUTA RAIZ DE LA CARPETA)\\sistema-de-proteccion-para-las-fuerzas-del-orden\\FOTOS_TFG\\' #CAMBIAR SEGÚN USUARIO
        output_folder = '(RUTA RAIZ DE LA CARPETA)\\sistema-de-proteccion-para-las-fuerzas-del-orden\\FOTOSARMA_TFG\\' #CAMBIAR SEGÚN USUARIO
        image_path = os.path.join(input_folder, image_file)
        image_path_out = os.path.join(output_folder, image_file)

        #Pasamos la foto para que OpenCV la comprenda
        frame = cv2.imread(image_path)
        #Ejecutamos la inteligencia artificial en cada foto
        detect = model(frame)
        #Guardamos la información obtneida de la detección
        info = detect.pandas().xyxy[0]

        # Verificar si hay coordenadas detectadas
        if not info.empty and any(info['confidence'].values > 0.4):
            print("\nARMA DETECTADA")

            #Mostramos por consola la información previamente almacenada
            print(info)
            #Guardamos las coordenadas de la detección
            x1 = info['xmin'].values.astype(int)
            y1 = info['ymin'].values.astype(int)
            x2 = info['xmax'].values.astype(int)
            y2 = info['ymax'].values.astype(int)

            for i in range(len(x1)):
                # Dibujamos el bounding box alrededor del objeto detectado
                cv2.rectangle(frame, (x1[i], y1[i]), (x2[i], y2[i]), (0, 255, 0), 2)  

            # Guarda la imagen con los recuadros pintados
            output_path = os.path.join(output_folder, image_file)
            cv2.imwrite(output_path, frame)

            #MQTT
            # Conectar al broker MQTT
            client.connect(broker, port)
            # Publicar un mensaje en el tema especificado
            client.publish(topic,1)
            client.disconnect()

            # Subir la imagen detectada a la tabla "fotosconarma"
            cursor = db.cursor()
            cursor2 = db.cursor()
            date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            queryfotos="SELECT idfotos FROM fotos WHERE fotos.ruta=%s"
            cursor2.execute(queryfotos,(image_path,))
            res= cursor2.fetchone()
            query = "INSERT INTO fotosconarma (idfotos,ruta, fecha) VALUES (%s,%s, %s)"
            cursor.execute(query, (res[0],image_path_out, date))
            db.commit()
            print("Foto insertada en la tabla 'fotosconarma':", image_path_out)

            # Cerrar los cursores y la conexión a la base de datos
            cursor.close()
            db.close()

            processed_images.append(image_file)
            print("imagenes procesadas:",processed_images,"\n")
        
    

if __name__ == '__main__':

    #Definimos las rutas de entrada y salida de imágenes
    input_folder = '(RUTA RAIZ DE LA CARPETA)\\sistema-de-proteccion-para-las-fuerzas-del-orden\\FOTOS_TFG\\' #CAMBIAR SEGÚN USUARIO
    output_folder = '(RUTA RAIZ DE LA CARPETA)\\sistema-de-proteccion-para-las-fuerzas-del-orden\\FOTOSARMA_TFG\\' #CAMBIAR SEGÚN USUARIO

    #Declaramos la cola de mensajes
    input_queue = mp.Queue()

    #Llamamos a la primera función en un proceso
    proceso_db = mp.Process(target=insert_into_database, args=(input_folder, input_queue))
    proceso_db.start()

    #Llamamos a la otra función en un proceso distinto al anterior
    proceso_detection = mp.Process(target=detect_objects, args=(input_queue, output_folder))
    proceso_detection.start()





