import torch
import cv2
import numpy as np

model = torch.hub.load('ultralytics/yolov5', 'custom', path='(RUTA RAIZ DE LA CARPETA)/sistema-de-proteccion-para-las-fuerzas-del-orden/IA/armas.pt')

cap = cv2.VideoCapture(0)

cv2.namedWindow('Detector de armas', cv2.WINDOW_NORMAL)  # Aumenta el tamaño de la ventana

while True:
    ret, frame = cap.read()
    detect = model(frame)
    info= detect.pandas().xyxy[0]
    print(info)
    # Obtener las coordenadas y confianzas de las detecciones

    # Mostrar cuadros de detección filtrados en la imagen original

    cv2.imshow('Detector de armas', np.squeeze(detect.render()))


    t = cv2.waitKey(1)  # Reduce el tiempo de espera

    if t == 27:
        break

cap.release()
cv2.destroyAllWindows()
