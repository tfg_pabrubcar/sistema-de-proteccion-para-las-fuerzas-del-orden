/* El presente código es parte de un trabajo de fin de grado. Grado en ingenieria informática - Ingeniería de computadores
 * Ha sido programado por Pablo Rubio Carballo. Versión Final. */


#include "esp_camera.h"
#include "soc/soc.h"           
#include "soc/rtc_cntl_reg.h"  
#include "driver/rtc_io.h"
#include <WiFi.h>
#include <WiFiClient.h>   
#include "ESP32_FTPClient.h"
#include <PubSubClient.h>
#include <NTPClient.h> 
#include <WiFiUdp.h>
#include "time.h"

//LAS SIGUIENTES VARIABLES DEBERÁN DE SER MODIFICADAS SEGÚN LOS PARÁMETROS DEL USUARIO
char* ftp_server = "IP DEL SERVIDOR"; 
char* ftp_user = "USUARIO FTP";
char* ftp_pass = "CONTRASEÑA FTP";
char* ftp_path = "RUTA DE LA CARPETA FTP";
const char* mqtt_server = "SERVIDOR MQTT";
const char* arma_topic = "TEMA MQTT";
const char* WIFI_SSID = "SSID WIFI";
const char* WIFI_PASS = "CONTRASEÑA WIFI";


WiFiClient espClient;
PubSubClient client(espClient);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", (3600), 60000);
ESP32_FTPClient ftp (ftp_server,ftp_user,ftp_pass, 5000, 2);

//Definición de los pines para CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM    -1
#define RESET_GPIO_NUM   -1
#define XCLK_GPIO_NUM    21
#define SIOD_GPIO_NUM    26
#define SIOC_GPIO_NUM    27

#define Y9_GPIO_NUM      35
#define Y8_GPIO_NUM      34
#define Y7_GPIO_NUM      39
#define Y6_GPIO_NUM      36
#define Y5_GPIO_NUM      19
#define Y4_GPIO_NUM      18
#define Y3_GPIO_NUM       5
#define Y2_GPIO_NUM       4
#define VSYNC_GPIO_NUM   25
#define HREF_GPIO_NUM    23
#define PCLK_GPIO_NUM    22
camera_config_t config;

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Conectado a red WiFi");
  Serial.println("Dirección IP: ");
  Serial.println(WiFi.localIP());
  
}


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Mensaje recibido en el tema: ");
  Serial.println(topic);
  Serial.print("Contenido: ");
  Serial.write(payload, length);
  Serial.println();

  // Analizar el mensaje recibido y realizar acciones según su contenido
  if (strcmp(topic, arma_topic) == 0) {
    if (payload[0] == '1') {
      digitalWrite(2, HIGH);  // Encender el LED
      Serial.println("Arma detectada");
      delay(500);
      digitalWrite(2, LOW);  // Apagar el LED
    } else if (payload[0] == '0') {
      digitalWrite(2, LOW);  // Apagar el LED
    }
  }
}


void reconnect() {
  while (!client.connected()) {
    Serial.print("Conectando al broker MQTT...");
    if (client.connect("ESP8266Client")) {
      Serial.println("Conectado!");
      client.subscribe(arma_topic);
    } else {
      Serial.print("Falló la conexión. Código de error: ");
      Serial.println(client.state());
    }
  }
}


void initCamera() {
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;


  //Definimos el tamaño y la calidad de la fotografía
  if(psramFound()){
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }  
  
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Fallo al iniciar la camara con error: 0x%x", err);
    return;
  }  
}



void takePhoto() {
  camera_fb_t * fb = NULL;
  
  // Take Picture with Camera
  fb = esp_camera_fb_get();  
  if(!fb) {
    Serial.println("Camera capture failed");
    return;
  }

  ftp.ChangeWorkDir(ftp_path);
  ftp.InitFile("Type I");

  String nombreArchivo = timeClient.getFullFormattedTimeForFile()+".jpg"; // AAAAMMDD_HHMMSS.jpg
  Serial.println("Subiendo "+nombreArchivo);
  int str_len = nombreArchivo.length() + 1; 
 
  char char_array[str_len];
  nombreArchivo.toCharArray(char_array, str_len);
  
  ftp.NewFile(char_array);
  ftp.WriteData( fb->buf, fb->len );
  ftp.CloseFile();
 
  esp_camera_fb_return(fb); 
}




void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  setup_wifi();
  initCamera();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  
  timeClient.begin();
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
  Serial.println("Tiempo sincronizado");
  Serial.println(timeClient.getFullFormattedTimeForFile());

  ftp.OpenConnection();
}

void loop() {
  timeClient.update();
  takePhoto();
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
}
