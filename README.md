# SISTEMA DE PROTECCION PARA LAS FUERZAS DEL ORDEN

Autor: Pablo Rubio Carballo

Estos archivos corresponden al Trabajo de Fin de Grado para la obtención del título de Grado en Ingeniería informática - Ingeniería de Computadores por la Universidad de Sevilla en el curso 2022/2023. Con calificación de 8,6.

En resumidas cuentas, es un sistema capaz de detectar armas de fuego o cuchillos en a través de una arquitectura cliente-servidor. 
El dispositivo cliente toma una captura de imagen que es enviada al servidor a través de la red. 
Dicho servidor tiene en ejecución un modelo de Inteligencia Artificial entrenado para la detección de armas junto a las coordenadas de la detección en la imagen. 
Tras ser detectado, se genera una copia de la imagen de entrada junto al recuadro donde ha sido identificada el arma.
Todas las imágenes son insertadas en una carpeta y las que copias que la IA detecta en otra.
Tras la detección positiva, el dispositivo cliente recibirá un mensaje indicando que se ha realizado una detección.

Gracias por su tiempo.
Un saludo,
Pablo.
