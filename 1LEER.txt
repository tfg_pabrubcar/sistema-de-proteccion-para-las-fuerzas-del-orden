La carpeta documentación contiene la Memoria y Presentación del proyecto para su defensa ante el tribunal.

En las distintas carpetas se encuentran los códigos correspondientes empleados para la realización del proyecto.
"FOTOS_TFG" y "FOTOSARMA_TFG" están vacías ya que es donde se insertarán las imágenes una vez configurado todo 
	el entorno, no es necesario emplear esas carpetas, pueden ser cualquier otras.

